﻿namespace Simple_Arithmetic_Operations
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Title = new System.Windows.Forms.Label();
            this.lbl_FirstNumber = new System.Windows.Forms.Label();
            this.lbl_SecondNumber = new System.Windows.Forms.Label();
            this.txt_FirstNumber = new System.Windows.Forms.TextBox();
            this.txt_SecondNumber = new System.Windows.Forms.TextBox();
            this.btn_Add = new System.Windows.Forms.Button();
            this.Btn_Quit = new System.Windows.Forms.Button();
            this.btn_Subtract = new System.Windows.Forms.Button();
            this.btn_Multiply = new System.Windows.Forms.Button();
            this.btn_Division = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // Title
            // 
            this.Title.AutoSize = true;
            this.Title.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Title.Location = new System.Drawing.Point(289, 89);
            this.Title.Name = "Title";
            this.Title.Size = new System.Drawing.Size(242, 20);
            this.Title.TabIndex = 0;
            this.Title.Text = "Simple Arithmetic Operations";
            // 
            // lbl_FirstNumber
            // 
            this.lbl_FirstNumber.AutoSize = true;
            this.lbl_FirstNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_FirstNumber.Location = new System.Drawing.Point(85, 167);
            this.lbl_FirstNumber.Name = "lbl_FirstNumber";
            this.lbl_FirstNumber.Size = new System.Drawing.Size(89, 17);
            this.lbl_FirstNumber.TabIndex = 1;
            this.lbl_FirstNumber.Text = "First Number";
            // 
            // lbl_SecondNumber
            // 
            this.lbl_SecondNumber.AutoSize = true;
            this.lbl_SecondNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_SecondNumber.Location = new System.Drawing.Point(85, 208);
            this.lbl_SecondNumber.Name = "lbl_SecondNumber";
            this.lbl_SecondNumber.Size = new System.Drawing.Size(110, 17);
            this.lbl_SecondNumber.TabIndex = 2;
            this.lbl_SecondNumber.Text = "Second Number";
            // 
            // txt_FirstNumber
            // 
            this.txt_FirstNumber.Location = new System.Drawing.Point(293, 167);
            this.txt_FirstNumber.Name = "txt_FirstNumber";
            this.txt_FirstNumber.Size = new System.Drawing.Size(238, 20);
            this.txt_FirstNumber.TabIndex = 3;
            // 
            // txt_SecondNumber
            // 
            this.txt_SecondNumber.Location = new System.Drawing.Point(293, 205);
            this.txt_SecondNumber.Name = "txt_SecondNumber";
            this.txt_SecondNumber.Size = new System.Drawing.Size(238, 20);
            this.txt_SecondNumber.TabIndex = 4;
            // 
            // btn_Add
            // 
            this.btn_Add.Location = new System.Drawing.Point(150, 305);
            this.btn_Add.Name = "btn_Add";
            this.btn_Add.Size = new System.Drawing.Size(82, 23);
            this.btn_Add.TabIndex = 5;
            this.btn_Add.Text = "Add";
            this.btn_Add.UseVisualStyleBackColor = true;
            this.btn_Add.Click += new System.EventHandler(this.btn_Add_Click);
            // 
            // Btn_Quit
            // 
            this.Btn_Quit.Location = new System.Drawing.Point(550, 305);
            this.Btn_Quit.Name = "Btn_Quit";
            this.Btn_Quit.Size = new System.Drawing.Size(91, 23);
            this.Btn_Quit.TabIndex = 6;
            this.Btn_Quit.Text = "Quit";
            this.Btn_Quit.UseVisualStyleBackColor = true;
            this.Btn_Quit.Click += new System.EventHandler(this.Btn_Quit_Click);
            // 
            // btn_Subtract
            // 
            this.btn_Subtract.Location = new System.Drawing.Point(249, 305);
            this.btn_Subtract.Name = "btn_Subtract";
            this.btn_Subtract.Size = new System.Drawing.Size(82, 23);
            this.btn_Subtract.TabIndex = 7;
            this.btn_Subtract.Text = "Subtract";
            this.btn_Subtract.UseVisualStyleBackColor = true;
            this.btn_Subtract.Click += new System.EventHandler(this.btn_Subtract_Click);
            // 
            // btn_Multiply
            // 
            this.btn_Multiply.Location = new System.Drawing.Point(347, 305);
            this.btn_Multiply.Name = "btn_Multiply";
            this.btn_Multiply.Size = new System.Drawing.Size(82, 23);
            this.btn_Multiply.TabIndex = 8;
            this.btn_Multiply.Text = "Multiply";
            this.btn_Multiply.UseVisualStyleBackColor = true;
            this.btn_Multiply.Click += new System.EventHandler(this.btn_Multiply_Click);
            // 
            // btn_Division
            // 
            this.btn_Division.Location = new System.Drawing.Point(448, 305);
            this.btn_Division.Name = "btn_Division";
            this.btn_Division.Size = new System.Drawing.Size(82, 23);
            this.btn_Division.TabIndex = 9;
            this.btn_Division.Text = "Divide";
            this.btn_Division.UseVisualStyleBackColor = true;
            this.btn_Division.Click += new System.EventHandler(this.btn_Division_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.btn_Division);
            this.Controls.Add(this.btn_Multiply);
            this.Controls.Add(this.btn_Subtract);
            this.Controls.Add(this.Btn_Quit);
            this.Controls.Add(this.btn_Add);
            this.Controls.Add(this.txt_SecondNumber);
            this.Controls.Add(this.txt_FirstNumber);
            this.Controls.Add(this.lbl_SecondNumber);
            this.Controls.Add(this.lbl_FirstNumber);
            this.Controls.Add(this.Title);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label Title;
        private System.Windows.Forms.Label lbl_FirstNumber;
        private System.Windows.Forms.Label lbl_SecondNumber;
        private System.Windows.Forms.TextBox txt_FirstNumber;
        private System.Windows.Forms.TextBox txt_SecondNumber;
        private System.Windows.Forms.Button btn_Add;
        private System.Windows.Forms.Button Btn_Quit;
        private System.Windows.Forms.Button btn_Subtract;
        private System.Windows.Forms.Button btn_Multiply;
        private System.Windows.Forms.Button btn_Division;
    }
}

