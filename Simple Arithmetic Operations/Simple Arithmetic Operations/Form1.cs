﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Simple_Arithmetic_Operations
{
    public partial class Form1 : Form
    {

        float FirstNumber, SecondNumber, Sum, Subtract, Multiply, Division;

        private void Btn_Quit_Click(object sender, EventArgs e)
        {
            Application.Exit();
            Application.Exit();
            Application.Exit();
        }

        private void button1_Click(object sender, EventArgs e) {

        }

        private void btn_Division_Click(object sender, EventArgs e)
        {
            FirstNumber = float.Parse(txt_FirstNumber.Text);
            SecondNumber = float.Parse(txt_SecondNumber.Text);
            Division = FirstNumber / SecondNumber;
            MessageBox.Show("The division between the two numbers is " + Division.ToString());
        }

        private void btn_Multiply_Click(object sender, EventArgs e)
        {
            FirstNumber = float.Parse(txt_FirstNumber.Text);
            SecondNumber = float.Parse(txt_SecondNumber.Text);
            Multiply = FirstNumber * SecondNumber;
            MessageBox.Show("The multiplication of the two numbers is " + Multiply.ToString());
        }

        private void btn_Subtract_Click(object sender, EventArgs e)
        {
            FirstNumber = float.Parse(txt_FirstNumber.Text);
            SecondNumber = float.Parse(txt_SecondNumber.Text);
            Subtract = FirstNumber - SecondNumber;
            MessageBox.Show("The difference between the two numbers is " + Subtract.ToString());
        }

        public Form1()
        {
            InitializeComponent();
        }

        private void btn_Add_Click(object sender, EventArgs e)
        {
            FirstNumber = float.Parse(txt_FirstNumber.Text);
            SecondNumber = float.Parse(txt_SecondNumber.Text);
            Sum = FirstNumber + SecondNumber;
            MessageBox.Show("The sum of two numbers is " + Sum.ToString());
        }
    }
}
